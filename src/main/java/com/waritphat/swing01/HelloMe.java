/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.swing01;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author domem
 */
public class HelloMe {

    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello ME");
        frmMain.setSize(500, 400);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JLabel lblName = new JLabel("Name: ");
        lblName.setSize(50, 20);
        lblName.setLocation(5, 5);

        JTextField fidName = new JTextField();
        fidName.setSize(150, 25);
        fidName.setLocation(70, 5);

        JButton btnHello = new JButton("Hello");
        btnHello.setSize(90, 30);
        btnHello.setLocation(20, 50);

        JButton btnGoodbye = new JButton("Goodbye");
        btnGoodbye.setSize(90, 30);
        btnGoodbye.setLocation(180, 50);

        JLabel lblHello = new JLabel();
        lblHello.setSize(200, 70);
        lblHello.setLocation(20, 90);
        
        JLabel lblGoodbye = new JLabel();
        lblGoodbye.setSize(200, 70);
        lblGoodbye.setLocation(20, 110);

        JButton btnReset = new JButton("Reset");
        btnReset.setSize(90, 30);
        btnReset.setLocation(100, 180);

        frmMain.setLayout(null);

        frmMain.add(lblName);
        frmMain.add(fidName);
        frmMain.add(btnHello);
        frmMain.add(btnGoodbye);
        frmMain.add(lblHello);
        frmMain.add(lblGoodbye);
        frmMain.add(btnReset);

        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = fidName.getText();
                lblHello.setText("Hello "+ name);
                lblHello.show();
            }

        });
        btnGoodbye.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = fidName.getText();
                lblGoodbye.setText("Goodbye "+ name);
                lblGoodbye.show();
            }

        });
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblHello.hide();
                lblGoodbye.hide();
            }

        });

        frmMain.setVisible(true);

    }
}
